import numpy as np
#seat mapping a time and memory control 
class input_data(object):
    subject={'A':59,'B':60,'C':150,'D':31,'E':48,'F':26,'G':41,'H':159,'I':200,'J':201,'K':22,'L':602,'M':650,'O':700}#key=subject code value=number of students
    subjectName=['K','F','D','G','E','A','B','C','H','I','J','L','M','O']
    room={'a1':{8:6},'a2':{7:6},'a3':{8:6},'a4':{6:5},'a5':{10:6},'a6':{7:6},'a7':{12:6},'c1':{15:6},
    'c2':{9:6},'c3':{7:6},'d1':{8:6},'d2':{7:6},'d3':{15:6},'d4':{6:4},'e1':{16:6},'e2':{17:6},'e3':{8:6},'e4':{6:5},'e5':{10:6},'e6':{7:6},'e7':{12:6},'f1':{6:6},
    'f2':{9:6},'f3':{7:6},'g1':{8:6},'g2':{7:6},'g3':{6:6},'g4':{6:4},'h1':{15:6},'h2':{7:6},'h3':{20:6},'h4':{6:4},'i1':{6:6},'i2':{7:6},'i3':{8:6},'i4':{6:5},'i5':{10:6},'i6':{7:6},'i7':{12:6},'j1':{6:6},
    'j2':{9:6},'j3':{7:6},'k1':{8:6},'k2':{15:10},'k3':{12:6},'k4':{15:8},'l':{8:6},'l2':{7:6},'l3':{6:6},'l4':{6:4},'m1':{6:6},'m2':{7:6},'m3':{8:6},'m4':{6:5},'m5':{10:6},'m6':{7:6},'m7':{12:6},'n1':{6:6},
    'n2':{9:6},'n3':{7:6},'o1':{8:6},'o2':{15:10},'o3':{12:6},'o4':{15:8}}
    #key=room no , value=column
    roomSize={}
    temp=[]#combination
    same_row={}
    key=[]
    som=0
    extraSeat=[0]##available extra Seat 
    preResult={}
    data=[] #max column for same subject students
    max_data={}
    max_num=[]
    o=0
    subjectColumn={}
    studentSize=[]
    num_student=np.array([])
    column_capacity={}#store the total number of different type of column

class Preparation(input_data):
    """extra seat control """
    cc=input_data()
    same_room={}
    same_Room=[]
    temproom=[]                #unused extra room
    def head(self):
        """main function in this class"""
        self.roomCapacity()
        self.sameRoom()
        self.extraSeatControl()
      
        #print "column_capacity",self.cc.data
        
   
    def T_Student(self):
        """calculate the Total number of students"""
        total_Student=0 
        for i in self.cc.subject:
            total_Student=total_Student+self.cc.subject[i]
        print "total_Student", total_Student
        return total_Student 
        
    def T_Seat(self):
        """calculate student capacity in room"""
        total_seat=0 
        for i in self.cc.roomSize:
            total_seat=total_seat+self.cc.roomSize[i]
        print "total_seat", total_seat
        return total_seat  
        
    def add_column(self,num):
        """calculate how mutch same size column """
        if (num in self.cc.column_capacity):
            self.cc.column_capacity[num]=self.cc.column_capacity[num]+1
        else:
            self.cc.column_capacity.setdefault(num, 1)
    def roomCapacity(self):
        #print self.cc.room
        for row in self.cc.room:
            for a in self.cc.room[row]:
                self.cc.roomSize.setdefault(row,a*self.cc.room[row][a])
    def sameRoom(self):
        #print "self.cc.roomSize",self.cc.roomSize
        for i in self.cc.roomSize:
            if self.cc.roomSize[i] in self.same_room:
                self.same_room[self.cc.roomSize[i]]=self.same_room[self.cc.roomSize[i]]+1
            else:
                self.same_room.setdefault(self.cc.roomSize[i],1)
                self.same_Room.append(self.cc.roomSize[i])
        #print self.same_room
        self.same_Room=sorted(self.same_Room)

    def extraSeatControl(self):############3/3/2015:9:15AM
        """determine extra room"""
        #print self.same_Room[0]
        delete=[]
        self.temp= self.T_Seat()-self.T_Student()
        if self.same_Room[0]<self.temp:
            result={}
            z=-1
            while len(result)<1:
                z=z+1
                result=self.roomDelete(self.temp-z,0)
            
            for x in result:
                xx=result[x]
                while xx>0:
                    xx=xx-1
                    for xxx in self.cc.roomSize:
                        if self.cc.roomSize[xxx]==x:
                            if xxx in delete:
                                """"""
                            else:
                                delete.append(xxx)
                                break
            self.temp=z
       
        for xxx in delete:
            del self.cc.roomSize[xxx]
        self.column_distribution()
        if self.same_Room[0]>self.temp and self.temp>=self.data[0]:
            self.extra_Seat(self.temp,0)
            #print "self.temp",self.temp
        else:
            self.cc.extraSeat[0]=self.temp
           # print "self.temp",self.data[0]
       # print "self.cc.extraSeat[0]",self.cc.extraSeat[0]
        
       
        """calculate extrea seat by using as like "self.roomDelete" function"""        
     
    def roomDelete(self,c_num,data):#c_num==extra seat & data = 0
        i=1
        while i<self.same_room[self.same_Room[data]]:
            if c_num-(i*self.same_Room[data])==0:
                return {self.same_Room[data]:i}
            elif c_num-(i*self.same_Room[data])<0:
                return {}
            elif data+1>=len(self.same_Room):
                """"""
            else:
                result=self.roomDelete(c_num-(i*self.same_Room[data]),data+1)
                if len(result)>0:
                    result.setdefault(self.same_Room[data],i)
                    return result
            i=i+1
        return {}
                    
    def extra_Seat(self,c_num,data):#c_num==extra seat & data = 0
       # print "c_num,data",c_num,data
        i=0
        while i<self.cc.max_data [self.cc.data[data]]:
            num=c_num-(i*self.cc.data[data])
            if num==0:
                 self.cc.extraSeat[0]=0
            elif data+1>=len(self.cc.data):
                """"""
            elif num>=0:
                result=self.extra_Seat(num,data+1)
                if result==0:
                    return 0
                if self.cc.extraSeat[0]==0 or self.cc.extraSeat[0]>num:
                    self.cc.extraSeat[0]=num
                    #print "self.cc.extraSeat[0]",self.cc.extraSeat[0]     
            else:
                break
                
                
        i=i+1
                

                                
  
            
    def column_distribution(self):
        """calculate the maximum column for any subject student"""

        for row in self.cc.roomSize:
            
            for a in self.cc.room[row]:
                i=0
                
                if a in self.cc.column_capacity:
                      self.cc.column_capacity[a]=self.cc.column_capacity[a]+self.cc.room[row][a]
                else:
                       self.cc.column_capacity.setdefault(a,self.cc.room[row][a]) 
                
                if self.cc.room[row][a]%2==0:
                    i=self.cc.room[row][a]/2
                else:
                    i=(self.cc.room[row][a]/2)+1
                #print "sdaufiluisdaf",self.cc.room[row][a]
                
                       
                if a in self.cc.max_data:
                      self.cc.max_data[a]=self.cc.max_data[a]+i
                else:
                       self.cc.max_data.setdefault(a,i) 
                       self.cc.data.append(a)  

                if(a in self.cc.same_row):
                    """cheak total same capacity column """
                    #print self.cc.room[row][a]
                    self.cc.same_row[a]=self.cc.same_row[a]+self.cc.room[row][a]
                else:
                     self.cc.same_row.setdefault(a,self.cc.room[row][a]) 
                #print "self.cc.same_row",self.cc.max_data 
       
        self.cc.data=sorted(self.cc.data)
        print "self.cc.max_data",self.cc.max_data 
        print "self.cc.same_row",self.cc.same_row      
        print "self.cc.data",self.cc.data
       
            


        

class seat_Combination(input_data):
    
    """processing of column combination"""
    
    cc=input_data()
    RP_temp={}
    Sum_store={}
    temp=[]#combination
    use={}
    def headRP(self):
        
        self.keyGeneration()
        
        self.extra()
       
        self.columnAllocation()
       
        
    def extra(self):
        for i in self.cc.max_data:
            self.cc.max_num.append(i)
            self.cc.som=self.cc.som+i*self.cc.max_data[i]
        self.cc.max_num=sorted(self.cc.max_num)
    def keyGeneration(self):
        #print self.cc.extraSeat[0]
        
        ii=0
        while ii<len(self.cc.subjectName):
            if ii==len(self.cc.subjectName)-1:
                num=self.cc.subject[self.cc.subjectName[ii]]+self.cc.extraSeat[0]
            else:
                 num=self.cc.subject[self.cc.subjectName[ii]]
            self.cc.subjectColumn.setdefault(num,{})
            self.cc.key.append(num)
            self.cc.studentSize.append(num)
            self.cc.num_student=np.append(self.cc.num_student,num)
            ii=ii+1
        self.cc.studentSize=sorted( self.cc.studentSize)
        self.cc.num_student=np.sort(self.cc.num_student)

   
    def columnAllocation(self):
        i=0
       # result=self.combination(803,self.cc.data,{})
        #print result
        total={}
        data=[]
        for ii in self.cc.data:
            data.append(ii)
            self.use.setdefault(ii,0)
        for ii in self.cc.same_row:
            total.setdefault(ii,self.cc.same_row[ii])
            
        #print total ,"total"  
        while i<len(self.cc.studentSize):
            use={}
            tem=[]
            result=self.combination(self.cc.studentSize[i],data,{})
            #print self.cc.studentSize[i],result
        
            for ii in result:
                self.cc.subjectColumn[self.cc.studentSize[i]].setdefault(ii,result[ii])
                self.cc.same_row[ii]=self.cc.same_row[ii]-result[ii]
                if self.cc.max_data[ii]>self.cc.same_row[ii]:
                   self.cc.max_data[ii]=self.cc.same_row[ii]
            for x in total:
                #print self.cc.same_row,total
                use.setdefault(x,((total[x]-self.cc.same_row[x])*100)/total[x])
                tem.append(((total[x]-self.cc.same_row[x])*100)/total[x])
            tem=sorted(tem)
            #print tem
            
            for x in use:
                if use[x] in tem:
                     tem[tem.index(use[x])]=x
                
            data=tem
            for jj in use:
                self.use.setdefault(jj,use[jj])
            #print use
                    

            i=i+1
        print self.cc.same_row
                
            
    def combination(self,num,column,result):
        #print num,column,result
        tem=[]
        a_clumn=[]
        a_result={}
        xxx=0
        if len(column)==0:
            yyy=0
            for i in result:
                if result[i]<self.cc.max_data[i]:
                    yyy=1
            if yyy==1:      
                return 0
            else:
                return 1
        elif num==0:
            return result
        else:
        
            for i in column:
                if num>i:
                    xxx=1
                if num%i==0 and num/i<=self.cc.max_data[i] and self.use[i]<50:
                    #print self.use[i]
                    result.setdefault(i,num/i)
                    return result
                
                #a_clumn.append(self.cc.same_row[i])
            if xxx==0:
                return 0
           # tem=sorted(tem)
            
                
            for i in column:
                a_clumn.append(i)
                tem.append(i)
            #print tem
                    
            
            temp=len(tem)
            n=0
            #print tem[0],temp

            while temp>n:
                
            # print a_clumn,tem[temp]
                a_clumn.remove(tem[n])
                for i in result:
                    a_result.setdefault(i,result[i])
                temp2=num/tem[n]
            # print tem
                if temp2>self.cc.max_data[tem[n]]:
                    temp2=self.cc.max_data[tem[n]]
                while temp2>=0:
                    a_result.setdefault(tem[n],temp2)
                    r_result=self.combination(num-temp2*tem[n],a_clumn,a_result)
                    if r_result!=0:
                        return r_result
                    del a_result[tem[n]]
                    temp2=temp2-1
                a_clumn.append(tem[n])
                n=n+1
            return 0
                
                
                
                
                
                
        

                
            
            
      
    
        



c=input_data() 
#print "start"
p=Preparation()
p.head()

a=seat_Combination()
a.headRP()

for i in c.subjectColumn:
   print i,c.subjectColumn[i]
"""for k in c.subjectColumn[i]:
        if 6 in c.subjectColumn[i][k]:
            print 6,c.subjectColumn[i][k][6]"""
#print len(c.subjectColumn)

#print z.cheak(5)

#for i in z.subject:
   # print i,z.subject[i]
#print c.max_data
"""for i in c.subjectColumn: 
    for j in c.subjectColumn[i]:
        print i,j,c.subjectColumn[i][j]"""
#print c.som
#for i in c.subjectColumn:
#print 67, c.subjectColumn[67]
#print 66, c.subjectColumn[66]
#print 69, c.subjectColumn[66]
"""     for j in c.subjectColumn[i]:
        if aa==c.subjectColumn[i][j]:
            print "ok","\n"

            
""" 
#for i in se
#print "c.data",c.data  

#print "roomSize", len(c.roomSize)                     
#print "c.max_data",c.max_data    
#print "same_row" ,c.same_row
"""
print "roomSize", c.roomSize
print "temp",c.temp
print "Sum_store", c.Sum_store
print "subjectColumn", c.subjectColumn

print "som", c.som
print "extraSeat", c.extraSeat
print "same_row" ,c.same_row
print "roomSize", c.roomSize

print "c.data",sorted(c.data)
"""