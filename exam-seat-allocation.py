class input_data(object):
    subject={'A':59,'B':60,'C':153,'D':31,'E':48,'F':26,'G':41}#key=subject code value=number of students
    subjectName=['F','D','G','E','A','B','C']
    room={'a1':[6,6,6,6,6],'a2':[7,7,7],'a3':[8,8,8,8],'a4':[6,6,6,6],'a5':[7,7,7,7,7],'a6':[7,7,7,7,7],'a7':[8,8,8,8],'c1':[10,10,10,10],
    'c2':[8,8,8,8,8],'c3':[8,8,8,8,8,8,8],'d1':[8,8,8,8,8,8],'d2':[7,7,7,7],'d3':[6,6,6,6,6],'d4':[8,8,8,8,8]}
    #key=room no , value=column
    roomSize={}
    final_sum_store={}
    same_row={}
    som=0
    extraSeat=[0]##available extra Seat 
    data=[] #max column for same subject students

    column_capacity={}#store the total number of different type of column


class Preparation(input_data):
    """extra seat control """
    cc=input_data()
    smallRoomCapacity=0
    temp=0;
    ar=[]                    #sorted room capacity
    sr=[]                    #sorted column capacity
    temproom=[]                #unused extra room
    def head(self):
        """main function in this class"""
        self.roomCapacity()
        #print self.cc.roomSize
        #self.column_distribution(self.cc.room)
        self.extraSeatControl()
        #print self.cc.roomSize
        self.column_distribution(self.cc.room)
        #self.column_distribution(self.cc.room)
        
        
    def T_Student(self):
        """calculate the Total number of students"""
        total_Student=0 
        for i in self.cc.subject:
            total_Student=total_Student+self.cc.subject[i]
        return total_Student 
        
    def T_Seat(self):
        #print self.T_Student()
        """calculate student capacity in room"""
        total_seat=0 
        for i in self.cc.roomSize:
            total_seat=total_seat+self.cc.roomSize[i]
        #print total_seat 
        return total_seat  
        
    def add_column(self,num):
        """calculate how mutch same size column """
        if (num in self.cc.column_capacity):
            self.cc.column_capacity[num]=self.cc.column_capacity[num]+1
        else:
            self.cc.column_capacity.setdefault(num, 1)
    def roomCapacity(self):
        for row in self.cc.room:
            self.cc.roomSize.setdefault(row,0)
            for a in self.cc.room[row]:
                self.cc.roomSize[row]=self.cc.roomSize[row]+a
              
        
            
    def column_distribution(self,room_):
        """calculate the maximum column for any subject student"""
        for row in self.cc.roomSize:
            i=0
            for a in self.cc.room[row]:
                if i==0:
                    """cheak same max student for any subject""" 
                    self.data.append(a)
                    self.add_column(a)
                    i=1
                else:
                    self.add_column(a)
                    i=0
                if(a in self.cc.same_row):
                    """cheak total same capacity column """
                    self.cc.same_row[a]=self.cc.same_row[a]+1
                else:
                     self.cc.same_row.setdefault(a,1)       
          
    def extraSeatControl(self):############3/3/2015:9:15AM
        """determine extra room"""
        
        self.temp= self.T_Seat()-self.T_Student()
        for i in self.cc.roomSize:
            self.ar.append(self.cc.roomSize[i])
        self.ar=sorted(self.ar)
       # print ar  
        for i in self.cc.same_row:
            self.sr.append(self.cc.same_row[i])   
        self.sr=sorted(self.sr)
        a=[0]
        result=self.extraRoom(self.temp,a)
       # print self.temp
        if result==0 and (not self.temproom or (self.temproom[0]==0 and not self.temproom[1])):
            xt=self.temp
            t_r=0#temp room for delect
            for i in self.ar:                    # extra seat matching with room
                if xt>(self.temp-i) and (self.temp-i)>-1:
                    xt=self.temp-i
                    t_r=i
            self.temp=xt 
            result=[t_r]                          #which room can be remove
            for i in self.sr:                    #friction calculation
                #print i
                if xt>(self.temp%i):
                    xt=self.temp%i
            self.temp=xt
        else:
            self.temp=0
            
       # print self.temproom
       # print result
        #print self.temproom                            ###***************delete     
       # print self.cc.roomSize
        if result:
            self.deleteRoom(result)
        elif self.temproom:
            self.deleteRoom(self.temproom)
        self.cc.extraSeat[0]=self.temp
        #print  self.cc.extraSeat
    
    
    def deleteRoom(self,room):
        """delete extra room"""
        ar=[]
        for i in self.cc.roomSize:#determine roo_no which can be delete
            for j in room:
                if j==self.cc.roomSize[i]:
                    ar.append(i)
                    room.remove(j)
                    continue
                
        for i in ar:#delete room
            del self.cc.roomSize[i]
        #print self.cc.roomSize
        #print ar                                           ##***************delete 
        
        


            
    def extraRoom(self,num,rooms):
        """how much extra room and which room can be remove"""
        xx=0
        for i in self.ar:
            if i in rooms:                #if room is alredy select for delete
                continue 
            else:                       # extra seat matching with room
                if num==i:
                    return [i]
                    
                if( num<i):             #friction calculation
                    for j in self.sr:
                        if num%j==0:
                            tem=0
                            for i in rooms:
                                tem=tem+i
                            for i in self.temproom:
                                tem=tem-i
                            if tem<1:
                                self.temproom=[]
                                for i in rooms:
                                   # print i
                                    self.temproom.append(i)
                            else:
                                self.temproom=[]
                                for i in rooms:
                                    self.temproom.append(i)
                            
                        else:
                            return 0
                else:
                    rooms.append(i)
                    value=[]
                    value=self.extraRoom(num-i,rooms)
                    rooms.remove(i)
                    if value==0:
                        continue
                    else:
                        val=[]
                        val=value
                        val.append(i)
                        return val
        return 0

        """end of extra seat control"""
                
        
class studentControl(input_data):
    cc=input_data() 
    
       

class room_post(input_data):
    
    """processing of column combination"""
    
    cc=input_data()
    RP_temp={}
    Sum_store={}
    temp=[]#combination
    def headRP(self):
        self.cc.data.sort()
        self.combination_seat(self.cc.data,100)
        self.sum_store_dic()
        self.FinalSum()
        n=100
        while(n>0):
            if n in self.Sum_store:
                del self.Sum_store[n]
            n=n-1
        del self.temp[:]

    def sum_store_dic(self):
        """insert into dictionary base on sum of combination as key"""
        
        #print self.temp 
        for item in self.temp:
            sums=0
            
            for value in item:
                sums=sums+value    
            if sums in self.Sum_store:
                self.Sum_store[sums].append(item)
            elif sums>=self.cc.data[0] and sums<101:
                self.Sum_store.setdefault(sums, [])
                self.Sum_store[sums].append(item)
           # print self.cc.Sum_store[sums]
                
            
        
        
        
         
    def FinalSum(self):
        """calculate how much column and which sizes of column needs"""
        
        for key in self.Sum_store:
            tem=1
            if key in self.cc.final_sum_store:
                """it ok"""
            else:
                 self.cc.final_sum_store.setdefault(key,{})
                 
            self.cc.final_sum_store[key].setdefault(tem,{})
            for value in self.Sum_store[key]:
                self.cc.final_sum_store[key].setdefault(tem,{})
                for val in value:
                    if val in self.cc.final_sum_store[key][tem]:
                        self.cc.final_sum_store[key][tem][val]=self.cc.final_sum_store[key][tem][val]+1 
                    else:
                        self.cc.final_sum_store[key][tem].setdefault(val,1)
                tem=tem+1
            
               
    
    def sum(self,num):
        """calculate sum of combination befor inserting dictionary"""
        sumn=0
        for v in num:
            sumn=sumn+v
        return sumn
    
    
    def insart(self,num,length1,max_size):
        """***001 insert in dictonary(sum_store) if the value is new"""
        self.temp.append([num])
        i=0
        while i<length1:
            if((sum(self.temp[i])+num)<=max_size):
                b=list(self.temp[i])
                b.extend([num])
                self.temp.append(b)
            i=i+1
                
                #print b
                
    def repeat(self,num,length1,max_size):
        """***002 insert dictonary(sum_store)  if the value is alredy used"""
        length2=len(self.temp)
        while length1<length2:
            if((sum(self.temp[length1])+num)<=max_size):
                b=list(self.temp[length1])
                b.extend([num])
                self.temp.append(b)
            length1=length1+1 
                        
    
    def combination_seat(self,seat,max_s):  
        """first step of combination of column"""
        seat.sort()
        #print seat
        tem=""
        length1=0
        length2=0
        for colum in seat: 
            if (tem==colum):  
                """cheak is this value of column is alredy used  ***002"""
                length_Now=len(self.temp)
                self.repeat(colum,length1,max_s)
                length1=length_Now
            else:
                length1=len(self.temp)          
                self.insart(colum,length1,max_s)
                
            tem=colum 
       # print self.temp      




a=room_post()     
c=input_data()  

p=Preparation()
p.head()
#room information processing befor column combination
a.headRP()
#make column combination
#add combination in dictonary key as sum of combination

#print c.temp
#for i in c.final_sum_store:
   # print i,c.final_sum_store[i],"\n"



#print c.temp

#for i in c.final_sum_store:
 #   print i,c.final_sum_store[i]
#print c.Sum_store
class optimal(input_data):
    cc=input_data()
   # print cc.Sum_store
      





optimal()

class seat_mapping(input_data):
    cc=input_data() 
   
      
    subject={}
    key={}
    def cheak(self ):
        for sub in self.cc.subjectName:
            #print sub
            self.key.setdefault(sub,[])
            if sub in self.key:
                for com in self.cc.final_sum_store[self.cc.subject[sub]]:
                    self.key[sub].append(com)
        for call in self.cc.subjectName:
            self.combination(call)    
 
        
    def combination(self,call):
        temp={}
        ij=1
        #print temp,"\n"
        if 1 in self.subject:
            for alls in self.key[call]:
                for com in self.subject:
                     iii=self.calculate(call,alls,com)
                     if iii==0:
                         """sds"""
                     elif call in iii:
                         #print iii,"\n"
                         temp.update({ij:iii})
                         #print temp,"\n"
                         ij=ij+1
            
            self.subject.clear()
            self.subject=temp
           # print self.subject,"\n\n"
            #print temp,"\n"
                    
        else:
            for alls in self.key[call]:
                self.subject.setdefault(alls,{})
                self.subject[alls].setdefault(call,alls)
                for i in self.cc.final_sum_store[self.cc.subject[call]][alls]:
                    self.subject[alls].update({i:self.cc.final_sum_store[self.cc.subject[call]][alls][i]})
                    #print i,self.cc.final_sum_store[self.cc.subject[call]][alls][i]
            #print self.subject
        
    def calculate(self,call,alls,com):
        temp={}
        
       # print "call",call
        #print "alls",alls
        #print "con",com
        #temp.setdefault(ii,{})
        temp.update({call:alls})
        for i in self.cc.final_sum_store[self.cc.subject[call]][alls]:
            #print i
            if i in self.subject[com]:
                temp2=self.subject[com][i]+self.cc.final_sum_store[self.cc.subject[call]][alls][i]
                if temp2>self.cc.column_capacity[i]:
                    return 0
                else:
                    temp.update({i:temp2})
            else:
                temp.update({i:self.cc.final_sum_store[self.cc.subject[call]][alls][i]})
            
                    
        for i in self.subject[com]:
            if i in temp:
                """t"""
            else:
                temp.update({i:self.subject[com][i]})      
        return temp  
           
       
                  
            
#z=seat_mapping()
#print z.cheak(5)
#z.cheak()
"""need"""
#print z.subject
"""need"""
