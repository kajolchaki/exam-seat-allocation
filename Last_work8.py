import numpy as np
class input_data(object):
    subject={'A':59,'B':60,'C':150,'D':31,'E':48,'F':26,'G':41,'H':159,'I':200,'J':201,'K':15,'L':402}#key=subject code value=number of students
    subjectName=['K','F','D','G','E','A','B','C','H','I','J','L']
    room={'a1':{6:6},'a2':{7:6},'a3':{8:6},'a4':{6:5},'a5':{10:6},'a6':{7:6},'a7':{12:6},'c1':{6:6},
    'c2':{9:6},'c3':{7:6},'d1':{8:6},'d2':{7:6},'d3':{6:6},'d4':{6:4},'e1':{6:6},'e2':{7:6},'e3':{8:6},'e4':{6:5},'e5':{10:6},'e6':{7:6},'e7':{12:6},'f1':{6:6},
    'f2':{9:6},'f3':{7:6},'g1':{8:6},'g2':{7:6},'g3':{6:6},'g4':{6:4},'h1':{8:6},'h2':{7:6},'h3':{6:6},'h4':{6:4},'i1':{6:6},'i2':{7:6},'i3':{8:6},'i4':{6:5},'i5':{10:6},'i6':{7:6},'i7':{12:6},'j1':{6:6},
    'j2':{9:6},'j3':{7:6},'k1':{8:6},'k2':{15:10},'k3':{12:6},'k4':{15:8}}
    #key=room no , value=column
    roomSize={}
    temp=[]#combination
    same_row={}
    som=0
    extraSeat=0##available extra Seat 
    data=[] #max column for same subject students
    max_data={}
    max_num=[]
    o=0
    subjectColumn={}
    studentSize=[]
    column_capacity={}#store the total number of different type of column

class Preparation(input_data):
    """extra seat control """
    cc=input_data()
    same_room={}
    same_Room=[]
    temproom=[]                #unused extra room
    def head(self):
        """main function in this class"""
        self.roomCapacity()
        self.sameRoom()
        self.extraSeatControl()
        
        #print "temp",self.cc.extraSeat
        
   
    def T_Student(self):
        """calculate the Total number of students"""
        total_Student=0 
        for i in self.cc.subject:
            total_Student=total_Student+self.cc.subject[i]
        print "total_Student", total_Student
        return total_Student 
        
    def T_Seat(self):
        """calculate student capacity in room"""
        total_seat=0 
        for i in self.cc.roomSize:
            total_seat=total_seat+self.cc.roomSize[i]
        print "total_seat", total_seat
        return total_seat  
        
    def add_column(self,num):
        """calculate how mutch same size column """
        if (num in self.cc.column_capacity):
            self.cc.column_capacity[num]=self.cc.column_capacity[num]+1
        else:
            self.cc.column_capacity.setdefault(num, 1)
    def roomCapacity(self):
        #print self.cc.room
        for row in self.cc.room:
            for a in self.cc.room[row]:
                self.cc.roomSize.setdefault(row,a*self.cc.room[row][a])
    def sameRoom(self):
        #print "self.cc.roomSize",self.cc.roomSize
        for i in self.cc.roomSize:
            if self.cc.roomSize[i] in self.same_room:
                self.same_room[self.cc.roomSize[i]]=self.same_room[self.cc.roomSize[i]]+1
            else:
                self.same_room.setdefault(self.cc.roomSize[i],1)
                self.same_Room.append(self.cc.roomSize[i])
        #print self.same_room
        self.same_Room=sorted(self.same_Room)

    def extraSeatControl(self):############3/3/2015:9:15AM
        """determine extra room"""
        print self.same_Room[0]
        delete=[]
        self.temp= self.T_Seat()-self.T_Student()
        if self.same_Room[0]<self.temp:
            result={}
            z=-1
            while len(result)<1:
                z=z+1
                result=self.roomDelete(self.temp-z,0)
            
            for x in result:
                xx=result[x]
                while xx>0:
                    xx=xx-1
                    for xxx in self.cc.roomSize:
                        if self.cc.roomSize[xxx]==x:
                            if xxx in delete:
                                """"""
                            else:
                                delete.append(xxx)
                                break
            self.temp=z
       
        for xxx in delete:
            del self.cc.roomSize[xxx]
        self.column_distribution()
        if self.same_Room[0]>self.temp and self.temp>=self.data[0]:
            self.extraSeat(self.temp,0)
            print "self.temp",self.temp
        else:
            self.cc.extraSeat=self.temp
            print "self.temp",self.data[0]
        print "self.cc.extraSeat",self.cc.extraSeat
        
       
        """calculate extrea seat by using as like "self.roomDelete" function"""        
     
    def roomDelete(self,c_num,data):#c_num==extra seat & data = 0
        if c_num==0:
            return 0
        elif c_num<0:
            return -1
        elif data>=len(self.same_Room):
            return 1
        else:
            i=1
            while i<self.same_room[self.same_Room[data]]:
                result=self.roomDelete(c_num-(i*self.same_Room[data]),data+1)
                
                if result==0:
                    return {self.same_Room[data]:i}
                elif result==-1:
                    return {}
                elif result==1:
                    """"""
                elif len(result)>0:
                    result.setdefault(self.same_Room[data],i)
                    return result
                i=i+1
            return {}
                    
    def extraSeat(self,c_num,data):#c_num==extra seat & data = 0
       # print "c_num,data",c_num,data
        if c_num==0:
            self.cc.extraSeat=0
            return 0
        elif data>=len(self.cc.data):
            """"""
        else:
            i=0
            while i<self.cc.max_data [self.cc.data[data]]:
                num=c_num-(i*self.cc.data[data])
                if num>=0:
                    result=self.extraSeat(num,data+1)
                    if result==0:
                        return 0
                    if self.cc.extraSeat==0 or self.cc.extraSeat>num:
                        self.cc.extraSeat=num
                        #print "self.cc.extraSeat",self.cc.extraSeat
                    
                else:
                    break
                
                
                i=i+1
            1                

                                
  
            
    def column_distribution(self):
        """calculate the maximum column for any subject student"""

        for row in self.cc.roomSize:
            
            for a in self.cc.room[row]:
                i=0
                if self.cc.room[row][a]%2==0:
                    i=self.cc.room[row][a]/2
                else:
                    i=(self.cc.room[row][a]/2)+1
                #print "sdaufiluisdaf",self.cc.room[row][a]
                if a in self.cc.max_data:
                      self.cc.max_data[a]=self.cc.max_data[a]+i
                else:
                       self.cc.max_data.setdefault(a,i) 
                       self.cc.data.append(a)  

                if(a in self.cc.same_row):
                    """cheak total same capacity column """
                    #print self.cc.room[row][a]
                    self.cc.same_row[a]=self.cc.same_row[a]+self.cc.room[row][a]
                else:
                     self.cc.same_row.setdefault(a,self.cc.room[row][a]) 
                #print "self.cc.same_row",self.cc.max_data 
       
        self.cc.data=sorted(self.cc.data)
        print "self.cc.max_data",self.cc.max_data 
        print "self.cc.same_row",self.cc.same_row      
        print "self.cc.data",self.cc.data
       
            



class seat_Combination(input_data):
    
    """processing of column combination"""
    
    cc=input_data()
    RP_temp={}
    Sum_store={}
    temp=[]#combination
    def headRP(self):
        
        self.keyGeneration()
        
        self.extra()
        print "self.cc.max_data",self.cc.max_data
        print "self.cc.extraSea",self.cc.extraSeat
        print "len(self.cc.max_num)",len(self.cc.max_num)
        self.columnAllocation(len(self.cc.max_num),{})
       
        
    def extra(self):
        for i in self.cc.max_data:
            self.cc.max_num.append(i)
            self.cc.som=self.cc.som+i*self.cc.max_data[i]
        self.cc.max_num=sorted(self.cc.max_num)
        
    def keyGeneration(self):
        for i in self.cc.subjectName:
            ii=0
            while ii<=self.cc.extraSeat:
                self.cc.subjectColumn.setdefault(self.cc.subject[i]+ii,{})
                self.cc.studentSize.append(self.cc.subject[i]+ii)
                ii=ii+1
            self.cc.studentSize=sorted( self.cc.studentSize)


   
    def columnAllocation(self,c_num,data):#combination-----------29/03/15
        #print "c_num,data",c_num,data
        a={}
        if c_num==0:
            sums=0
            t=0
            aa=0
            cc=0
            dataa={}
            for i in data:
                sums=sums+i*data[i]
                if data[i]>0:
                    dataa.setdefault(i,data[i])
                if data[i]==1 or data[i]==2:
                    t=t+1
                if data[i]<self.cc.max_data[i]/5:
                    aa=aa+1
            
            bb=float(sums)/self.cc.som*1.1
            #print bb
            for i in dataa:
               if dataa[i]>(self.cc.max_data[i]*bb):
                   cc=cc+1
                    #t=t+1     
                
            if sums in self.cc.subjectColumn:
                if len(self.cc.subjectColumn[sums])>5 and t>0:
                    """"""
                elif len(self.cc.subjectColumn[sums])>40 and aa>0:
                    """"""
                elif len(self.cc.subjectColumn[sums])>100 and  cc>1:
                    """"""
                else:
                    self.cc.subjectColumn[sums].setdefault(len(self.cc.subjectColumn[sums])+1,dataa)
               # print sums,len(self.cc.subjectColumn[sums]),self.cc.subjectColumn[sums][len(self.cc.subjectColumn[sums])],"\n"
            return 0
            
        else:
            num=self.cc.max_data[self.cc.max_num[c_num-1]]
            xi=0
            for i in data:
                a.setdefault(i,data[i])
            a.setdefault(self.cc.max_num[c_num-1],0)
            while num>=xi:
                sums=0
                a[self.cc.max_num[c_num-1]]=xi
                for i in a:
                    sums=sums+i*a[i]
                if sums> self.cc.studentSize[len( self.cc.studentSize)-1]:
                    return 
                self.columnAllocation(c_num-1,a)
                xi=xi+1
           # print "columnAllocation",self.cc.max_num[c_num-1],num
             

        



c=input_data() 
#print "start"
p=Preparation()
p.head()
a=seat_Combination()
a.headRP()
#aaaaa=0
#for i in c.roomSize:
#    aaaaa=aaaaa+c.roomSize[i]
for i in c.subjectColumn:
    print i,len(c.subjectColumn[i])
#print c.max_data
"""for i in c.subjectColumn: 
    for j in c.subjectColumn[i]:
        print i,j,c.subjectColumn[i][j]"""
#print c.som
#for i in c.final_sum_store:
#print 67, c.final_sum_store[67]
#print 66, c.final_sum_store[66]
#print 69, c.final_sum_store[66]
"""     for j in c.final_sum_store[i]:
        if aa==c.final_sum_store[i][j]:
            print "ok","\n"

            
""" 
#for i in se
#print "c.data",c.data  

#print "roomSize", len(c.roomSize)                     
#print "c.max_data",c.max_data    
#print "same_row" ,c.same_row
"""
print "roomSize", c.roomSize
print "temp",c.temp
print "Sum_store", c.Sum_store
print "final_sum_store", c.final_sum_store

print "som", c.som
print "extraSeat", c.extraSeat
print "same_row" ,c.same_row
print "roomSize", c.roomSize

print "c.data",sorted(c.data)
"""